#!/bin/bash

set -xe

[ -z "$HOCKEY_APP_TOKEN" ]  && echo "Need to set HOCKEY_APP_TOKEN" && exit 1;
[ -z "$HOCKEY_APP_ID" ]     && echo "Need to set HOCKEY_APP_ID" && exit 1;
[ -z "$VERSION_CODE" ]      && echo "Need to set VERSION_CODE" && exit 1;
[ -z "$VERSION_NAME" ]      && echo "Need to set VERSION_NAME" && exit 1;


cd ../android && ./gradlew assembleRelease -p . -PversionCode=$VERSION_CODE -PversionName=$VERSION_NAME

cd app/build/outputs/apk

curl \
  -F "status=2" \
  -F "notify=1" \
  -F "notes=Some new features and fixed bugs." \
  -F "notes_type=0" \
  -F "apk=@app-release.apk" \
  -H "X-HockeyAppToken: $HOCKEY_APP_TOKEN" \
  https://rink.hockeyapp.net/api/2/apps/$HOCKEY_APP_ID/app_versions/upload